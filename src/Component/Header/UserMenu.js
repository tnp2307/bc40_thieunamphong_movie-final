import React from "react";
import { DownOutlined } from "@ant-design/icons";
import { Button, Dropdown, Space } from "antd";
import { useSelector } from "react-redux";
import { localStore } from "../../service/localUserStorage";
import { NavLink } from "react-router-dom";

const UserMenu = () => {
  let userInfor = useSelector((state) => {
    return state.userSlice.userInfor;
  });

  let handleLogout = () => {
    localStore.remove();
    window.location.reload();
    // window.location.href = "/login";
  };
  let renderMenu = () => {
    let buttonCss = "px-5 py-2 border-2 border-black";
    if (userInfor) {
      return (
        <Dropdown
          menu={{
            items: [
              { label: <span>Detail </span>, key: "0" },
              { label: <button onClick={handleLogout}>Logout</button> },
            ],
          }}
          trigger={["click"]}
        >
          <a onClick={(e) => e.preventDefault()}>
            <Space>
              {userInfor.hoTen}
              <DownOutlined />
            </Space>
          </a>
        </Dropdown>
      );
    } else {
      return (
        <>
          <div className="flex justify-between items-center gap-5">
            <form>
              <label
                htmlFor="default-search"
                className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white"
              >
                Search
              </label>
              <div className="relative">
                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                  <svg
                    aria-hidden="true"
                    className="w-5 h-5 text-gray-500 dark:text-gray-400"
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                    />
                  </svg>
                </div>
                <input
                  type="search"
                  id="default-search"
                  
                  placeholder="Search movie"
                  
                  required
                />
              </div>
            </form>

            <h5 className="text-right">
              Don’t have account yet ?
              <br />
              <a className="text-right" href="#">
                Please sign up here!
              </a>
            </h5>
            <NavLink to={"/login"}>
              <button
                className={buttonCss}
                style={{
                  background:
                    "linear-gradient(154.49deg, #9580FF 15.16%, rgba(149, 128, 255, 0.39) 83.85%)",
                  borderRadius: "16px",
                }}
              >
                Login
              </button>
            </NavLink>
          </div>
        </>
      );
    }
  };
  return renderMenu();
};

export default UserMenu;
