import { localStore } from "../../service/localUserStorage"
import { DANGNHAP } from "../constance/constance"

const initialState = {
    userInfor:localStore.get(),

}

export default (state = initialState, { type, payload }) => {
  switch (type) {

  case DANGNHAP:

    return { ...state,userInfor:payload }

  default:
    return state
  }
}
