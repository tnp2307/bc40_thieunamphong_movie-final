import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
};

const spinnerSlice = createSlice({
  name: "spinnerSlice",
  initialState,
  reducers: {
    setLoadingOn: (state, action) => {
      state.isLoading = true;
      console.log(state.isLoading);
    },
    setLoadingOff: (state, action) => {
      state.isLoading = false;
      console.log(state.isLoading);
    },
  },
});

export const { setLoadingOn, setLoadingOff } = spinnerSlice.actions;

export default spinnerSlice.reducer;
