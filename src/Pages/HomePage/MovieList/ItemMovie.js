import React, { useState } from "react";
import { Button, Card } from "antd";
import { NavLink } from "react-router-dom";
import ModalVideo from "react-modal-video";
import { useDispatch } from "react-redux";
import { setOpenModal } from "../../../reduxToolKit/movieModalSlice";
import MovieModal from "../../../Component/MovieModal/MovieModal";
const { Meta } = Card;
const ItemMovie = (props) => {
  const { hinhAnh, tenPhim, moTa, maPhim, trailer } = props.movie;
  let dispatch = useDispatch();
  let handleOpenModal = (trailer) => {
    dispatch(setOpenModal(trailer));
  };
  return (
    <div className="relative">
      <NavLink className=" hover:no-underline " to={`/detail/${maPhim}`}>
        <Card
          className="z-1 overflow-hidden"
          hoverable
          style={{
            width: 240,
          }}
          cover={
            <div
              className=" h-72 relative  "
              style={{
                backgroundImage: `url(${props.movie.hinhAnh})`,
                backgroundSize: "cover",
              }}
            ></div>
          }
        >
          <Meta
            className=""
            style={{ height: 78 }}
            title={tenPhim}
            description={moTa}
          />
        </Card>
      </NavLink>
      <Button
        className=" absolute left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2 transition duration-300 z-50 h-20 flex items-center justify-center border-none shadow-none opacity-0 play-button text-red-600 "
        onClick={() => handleOpenModal(trailer)}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          stroke-width="1.1"
          stroke="currentColor"
          class="hover:opacity-60 hover:z-30 transition duration-300 w-20 h-20"
        >
          <path
            stroke-linecap="round"
            stroke-linejoin="round"
            d="M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
          />
          <path
            stroke-linecap="round"
            stroke-linejoin="round"
            d="M15.91 11.672a.375.375 0 010 .656l-5.603 3.113a.375.375 0 01-.557-.328V8.887c0-.286.307-.466.557-.327l5.603 3.112z"
          />
        </svg>
      </Button>
    </div>
  );
};
export default ItemMovie;
