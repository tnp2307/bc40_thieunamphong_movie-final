import React, { useEffect, useState } from "react";
import { movieSer } from "../../../service/movieService";
import ItemMovie from "./ItemMovie";
import { useDispatch } from "react-redux";
import {
  setLoadingOff,
  setLoadingOn,
} from "../../../reduxToolKit/spinnerSlice";
import Spinner from "../../../Component/Spinner/Spinner";

export default function () {
  const [movies, setMovies] = useState([]);
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(setLoadingOn());
    movieSer
      .getMovieList()
      .then((res) => {
        console.log(res);
        setMovies(res.data.content);
        dispatch(setLoadingOff());
      })
      .catch((err) => {
        dispatch(setLoadingOff());
        console.log(err);
      });
  }, []);
  return (
    <div className="container grid grid-cols-1 xl:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-10 mx-auto">
    
      {movies.map((movie) => {
        return <ItemMovie movie={movie} key={movie.maPhim} />;
      })}
    </div>
  );
}
