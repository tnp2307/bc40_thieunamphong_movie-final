import React from "react";
import MovieList from "./MovieList/MovieList";
import CinemaTab from "./CinemaTab/CinemaTab";
import MovieModal from "../../Component/MovieModal/MovieModal";


export default function HomePage() {
  return (
    <div>
      <MovieList />
      <CinemaTab />
      <MovieModal/>
    </div>
  );
}
