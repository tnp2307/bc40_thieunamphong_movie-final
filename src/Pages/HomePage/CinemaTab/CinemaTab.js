import React, { useEffect, useState } from "react";
import { Radio, Space, Tabs } from "antd";
import { movieSer } from "../../../service/movieService";

import ItemMovie from "../MovieList/ItemMovie";
import ItemTab from "./ItemTab";
import { setLoadingOff, setLoadingOn } from "../../../reduxToolKit/spinnerSlice";
import { useDispatch } from "react-redux";

const CinemaTab = () => {
  const dispatch = useDispatch()
  const [heThongRap, setHeThongRap] = useState([]);
  useEffect(() => {
    dispatch(setLoadingOn());
    movieSer
      .getMovieByTheater()
      .then((res) => {
        
        setHeThongRap(res.data.content);
        dispatch(setLoadingOff());
      })
      .catch((err) => {
      
        dispatch(setLoadingOff());
      });
  }, []);
  const renderCinemaList = () => {
    return heThongRap.map((heThong) => {
      return {
        label: <img className="h-16 w-16" src={heThong.logo} />,
        // key: <imgs /> heThong.maHeThongRap,
        key: heThong.logo,
        children: (
          <Tabs
            style={{ height: 500 }}
            tabPosition="left"
            defaultActiveKey="1"
            items={heThong.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.tenCumRap,
                label: <div>{cumRap.tenCumRap}</div>,
                children: (
                  <div style={{ height: 500 }} className="overflow-y-scroll">
                    {cumRap.danhSachPhim.slice(0, 9).map((phim) => {
                      return <ItemTab phim={phim} key={phim.maPhim} />;
                    })}
                  </div>
                ),
              };
            })}
          />
        ),
      };
    });
  };

  return (
    <>
      <Tabs className="container py-20" style={{ height: 650 }} defaultActiveKey="1" tabPosition="top" items={renderCinemaList()} />
    </>
  );
};
export default CinemaTab;
