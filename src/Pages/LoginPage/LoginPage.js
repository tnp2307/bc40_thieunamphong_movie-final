import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { userService } from "../../service/userService";
import { Navigate, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { DANGNHAP } from "../../redux/constance/constance";
import { localStore } from "../../service/localUserStorage";
import { setLoginAction } from "../../reduxToolKit/userSlice";

const LoginPage = () => {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const onFinish = (values) => {
    userService
      .postLogin(values)
      .then((res) => {
        console.log(res);
        
        dispatch(setLoginAction(res.data.content))
        localStore.set(res.data.content)
        message.success("Login successful");
        navigate("/homepage");
      })
      .catch((err) => {
        console.log(err);
        message.error("Login fail");
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <Form
      name="basic"
      labelCol={{
        span: 8,
      }}
      wrapperCol={{
        span: 16,
      }}
      style={{
        maxWidth: 600,
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
      layout="vertical"
    >
      <Form.Item
        label="Username"
        name="taiKhoan"
        rules={[
          {
            required: true,
            message: "Please input your username!",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="matKhau"
        rules={[
          {
            required: true,
            message: "Please input your password!",
          },
        ]}
      >
        <Input.Password />
      </Form.Item>
      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Button
          className="text-white bg-blue-600 hover:bg-white hover:text-black"
          htmlType="submit"
        >
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};
export default LoginPage;
