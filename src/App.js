import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import LoginPage from "./Pages/LoginPage/LoginPage";
import HomePage from "./Pages/HomePage/HomePage";
import Layout from "./Layout/Layout";
import MovieDetail from "./Pages/MovieDetail/MovieDetail";
import ModalVideo from "react-modal-video";
import Spinner from "./Component/Spinner/Spinner";

function App() {
  return (
    <div>
      <Spinner/>
      <BrowserRouter>
        <Routes>
          <Route path="/login" element={<LoginPage/>} />
          <Route path="/homepage" element={<Layout Component={HomePage}/>} />
          <Route path="/detail/:id" element={<Layout Component={MovieDetail}/>} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
